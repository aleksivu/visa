<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tietovisa</title>
</head>
<body>
    <p>
        <?php
        $vastaus = filter_input(INPUT_POST, "vastaus", FILTER_SANITIZE_STRING);

        print "Vastasit $vastaus<br>";
        print "Vastauksesi on ";

        if($vastaus === "paviaani") {
            print "oikein";
        }
        else {
            print "väärin";
        }
        ?>
    </p>
    <a href="index.php">Yritä uudelleen</a>
    
</body>
</html>